# VX Linux
A pre-configured, secure systemd-free Plasma desktop with focus on convenience, performance and simplicity. Based on the excellent Void Linux. Uses X11 only because Wayland is nowhere near ready.

# Features
- Appmaker: Create web apps (aka SSBs) for your favourite online services
- Autoconfig: Automatically optimises your system
- Builder: Create your own Void-based distro
- Cleanup: Keep your system clean with a few clicks
- Fontsizer: Change font size across desktop, editor and terminal
- Installer: Install the current running system to disk and make bootable
- Modules: Install popular software and packages that requires special setup
- Packages: Manage packages with OctoXBPS
- Pipewire pre-installed (with fix for Intel dual audio)
- Restore: Restore a full system snapshot and make bootable
- Snapshot: Create a full system snapshot
- Tweak: Tweak your system with a few clicks
- Updater: Manage system updates with a few clicks

# Minimum Requirements
- x86_64 Processor
- 2GB Memory
- 6GB Space

# Download
https://mega.nz/folder/SxwFTCTZ#s_qwDWrSWr7ZSTBVJC9Q_g

# Known Issues
- AppMaker: First load of apps takes a long time due to initial caching of the resource, subsequent loads will be near instant
- Builder: First build is very slow due to downloading the void base. Packages are cached and subsequent builds will be much faster
